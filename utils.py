#! env python
# -*- coding: utf-8 -*-
# PyConsole.model
__author__ = 'mm'

import os
import sqlite3
import time
import configparser


def h(input_char=""):
    """
    https://stackoverflow.com/questions/931423/is-there-a-python-equivalent-to-the-php-function-htmlspecialchars
    htmlspecialchars()と同じ
    :param input_char: 入力値
    :type input_char: str
    :return:
    """
    if not isinstance(input_char, str):
        return input_char
    return input_char.replace("&", "&amp;").replace('"', "&quot;").replace("<", "&lt;").replace(">", "&gt;")


class Config(object):
    def __init__(self, path="config.ini", section="root", value={}, encode="utf-8"):
        """
        :param path: ファイルパス
        :type path: str
        :param section: 対象セクション名
        :type section: str
        :param value: 値一覧
        :type value: dict
        :param encode: エンコード
        :type encode: str
        """
        self.__path = path
        self.__section = section
        self.__data = value
        self.__encode = encode
        self.read()
        pass

    def set_encode(self, encode):
        self.__encode = encode

    def set_section(self, value):
        """
        :param value: セクション名
        :type value: str
        """
        self.__section = value

    def get_section(self):
        """
        :return: セクション名
        :rtype: str
        """
        return self.__section

    def __setitem__(self, key, value):
        """
        a[key] = value
        :param key: キー
        :type key: str
        :param value:
        :return:
        """
        self.__data[key] = value

    def __getitem__(self, key):
        """
        b = a[key]
        :param key: キー
        :type key: str
        :return: 値
        """
        return self.__data[key]

    def __delitem__(self, key):
        """
        del a[key]
        :param key: キー
        :type key: str
        """
        del self.__data[key]

    def get_int(self, key):
        """
        :param key: キー
        :return: 値
        :rtype: int
        """
        return int(self.__data[key])

    def get_bool(self, key):
        """
        :param key: キー
        :return: 値
        :rtype: bool
        """
        if self.__data[key] == "yes" or self.__data[key] == "on" or self.__data[key] == "1" or \
                self.__data[key] == 1 or self.__data[key]:
            return True
        elif self.__data[key] == "no" or self.__data[key] == "off" or self.__data[key] == "0" or \
                self.__data[key] == 0 or not self.__data[key]:
            return False
        return None

    def get_float(self, key):
        """
        :param key: キー
        :return: 値
        :type: float
        """
        return float(self.__data[key])

    def get_str(self, key):
        """
        :param key: キー
        :return: 値
        :rtype: str
        """
        return str(self.__data[key])

    def get(self):
        """
        :return: 値
        :rtype: dict
        """
        return self.__data

    def set(self, value={}):
        """
        :param value: 値
        :type value: dict
        """
        if isinstance(value, dict):
            self.__data = value

    def add(self, value={}):
        """
        :param value: 値
        :type value: dict
        """
        if isinstance(value, dict):
            self.__data.update(value)

    def read(self):
        """
        ファイル読み込み
        """
        if not os.path.exists(self.__path):
            self.write()
            return
        parser = configparser.ConfigParser()
        parser.read(self.__path, self.__encode)
        # 読み込んだファイルに対象セクションがあれば値を読み込む
        if self.__section in parser.sections():
            self.__data = {key: parser[self.__section][key] for key in parser[self.__section]}

    def write(self):
        """
        ファイル書き出し
        """
        if len(self.__data) != 0 and self.__path != "":
            # ステータスファイルを書き出す場合
            parser = configparser.ConfigParser()
            if os.path.exists(self.__path):
                # ファイルが存在する場合、対象セクション以外も取り込むために読み込む
                parser.read(self.__path, self.__encode)
            # データ上書き
            parser[self.__section] = self.__data
            with open(self.__path, 'w', encoding=self.__encode) as config_file:
                parser.write(config_file)
            # 書き込みタイミング調整用
            time.sleep(0.1)


class Model(object):
    """
    models = Model("test.db")
    models.execute("CREATE TABLE patient (id INTEGER PRIMARY KEY,status TEXT,patient_id TEXT,date TEXT)")
    models.execute("CREATE TABLE log (id INTEGER PRIMARY KEY,status TEXT,content TEXT,date TEXT,time TEXT,method TEXT)")
    models.select("select * from patient;")
    cursor = models.select("select * from patient limit 10;")
    for i in cursor:
        print(i["patient_id"])
    models.execute("insert into patient (patient_id) values (\"000\");")
    """

    def __init__(self, file_path):
        self.__ROOT = os.path.dirname(os.path.abspath(__file__))
        self.__file_path = file_path
        self.__init()
        return

    def __init(self):
        if not os.path.isabs(self.__file_path):
            self.__file_path = os.path.join(self.__ROOT, self.__file_path)
        if os.path.exists(self.__file_path) and self.__file_path != "":
            return
        # https://msrx9.bitbucket.io/blog/html/2013/07/04/db_study.html
        # os.path.exists()

    def __connect(self, sql):
        """
        result ----
        [
            {"column1": value1, "column2", value2},
            {"column1": value3, "column2", value4}
        ]
        -----------
        :param sql:
        :return:
        """
        # self.conn = sqlite3.connect(self.__init_file_path)
        conn = sqlite3.connect(self.__file_path)
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute(sql)
        result = []
        for row in cur:
            result.append(row)
        conn.close()
        return result

    def select(self, sql=None):
        var_sql = sql
        if sql is None:
            var_sql = ""
        return self.__connect(var_sql)
        # return self.cursor.fetchall()

    def execute(self, sql):
        """
        commitがある。
        :param sql:
        :return:
        """
        var_sql = sql
        if sql is None:
            var_sql = ""
        conn = sqlite3.connect(self.__file_path)
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute(var_sql)
        conn.commit()
        conn.close()
        return True

    def is_table(self, table=""):
        result = self.select("""select name from sqlite_master where type='table' and name = '{}';""".format(table))
        return len(result) != 0

    def create(self, sql, table=""):
        """
        テーブル名を検索して存在していれば作成しない
        :param sql:
        :param table:
        :return:
        """
        var_sql = sql
        if sql is None:
            var_sql = ""
        if not os.path.isabs(self.__file_path):
            self.__file_path = os.path.join(self.__ROOT, self.__file_path)
        result = self.select("""
        select name from sqlite_master where type='table' and name = '{}';
        """.format(table))
        if len(result) != 0:
            return False
        conn = sqlite3.connect(self.__file_path)
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute(var_sql)
        conn.close()
        return True
