# Todo 修正したいところとか

- [ ] change in.
- [x] Success

# Overview 概要

PyConsole.readme
Date: 2018/08/31

# Description 詳細

Pythonのbottleを使ったWebApplicationのテンプレート。

# create envs. 仮想環境の作り方

Assuming that conda is installed.
Condaがインストールされていることが前提。

```commandline
set CONDA_FORCE_32BIT=1
```

```commandline
conda create -n PyConsole python=3.6.7
# # + Anaconda
# conda create -n PyConsole python=3.6.7 anaconda
# # informaiton
# conda info -e
# conda list -n PyConsole
# # remove env
# conda remove -n PyConsole --all
```

# Activation of Python environment. 仮想環境に入る


```bash:for windows
activate PyConsole
```


```bash:for mac
source activate PyConsole
```

# Requirement / install package. パッケージのインストール

```bash
pip install bottle
pip install jinja2
pip install beaker
# https
pip install gevent
# 半角全角
# pip install mojimoji
pip install zenhan
# pip install logzero
pip install pysmb
pip install bottle-login
```

# Demo / Usage 使用例

main.pyを実行して動作開始。

```commandline
# Execution command.Be sure to activation.
python main.py
```

## SSLキー作成

> https://blog.motikan2010.com/entry/2017/01/11/Python%E3%81%A0%E3%81%91%E3%81%A7HTTPS%E3%82%B5%E3%83%BC%E3%83%90

```bash
cd 鍵保存フォルダ
openssl req -new -x509 -keyout server.pem -out server.pem -days 365 -nodes
# 入力内容
# Country Name (2 letter code) [AU]:JP
# State or Province Name (full name) [Some-State]:Tokyo
# Locality Name (eg, city) []:Minato-ku
# Organization Name (eg, company) [Internet Widgits Pty Ltd]:TESTS
# Organizational Unit Name (eg, section) []:TS
# Common Name (e.g. server FQDN or YOUR name) []:localhost
# Email Address []:
```

# ファイル構成

|ファイル/フォルダ名|コメント|
|---:|:---|
|db|データベースの保管用フォルダ|
|htdocs|HTMLファイルとか保管|
|config.ini|httpの設定|
|index.bsdesign|Bootstrap studio用のファイル|
|main.py|bottleの設定とかの設定ファイル。<br/>このファイルをpythonで実行すると開始|
|models.py|データベースにアクセスしやすくするためのクラスファイル。|
|user_model.py|ログイン関係の処理用ファイル|
|utils.py|補助用ファイル|

# 設定

main.py内の定数/変数を変更して動作を変更出来ます。

|値名|例|効果|
|---:|:---|:---|
|user_path|"db/users.db"|ユーザーのデータベースのファイルパス|
|IS_SSL|True/False|SSLを使うかどうか。<br/>server.pemが無いと動作しませんので「SSLキー作成」を例に作成。|
|IS_ASYNCHRONOUS|True/False|Trueにすると非同期動作になる。|
|CONTENTS_DIR|"htdocs"|HTML等のファイルの保存ディレクトリ名|
|IS_LOGIN|True/False|Trueでログイン機能を有効にする。|