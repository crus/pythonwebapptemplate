#! env python
# -*- coding: utf-8 -*-
"""
bottleの使い方
@root.route("/login", method="POST") <- どのようなルールのリクエストが来たか
def example(): <- 関数名に意味はない。被っても動作するが、できればそれぞれ違う関数名にする。
    return "hello world" <- 返り値がそのままレスポンスとしてクライアントに返ってくる。HTMLを表示したいならHTMLを、JSONを取得したいなら、json.dumps({キー:値})のようにする

@route('/path/<arg1>/<arg2>/<arg3:re:[a-z]+>', method='POST') <- 引数を設定することもできる
def 何か関数名(arg, arg2, arg3):
    内容
    return template("index") <- index.htmlをテンプレート様式（今回はJINJA2)で表示させる。

ログイン関連
http://www.shido.info/py/bottle_session.html
クライアントのIPアドレス取得
https://stackoverflow.com/questions/31405812/how-to-get-client-ip-address-using-python-bottle-framework
ポートスキャン
https://qiita.com/najayama/items/728682bcae824c902046
"""

import os
import sys
import codecs

sys.path.append(os.path.dirname(os.path.abspath(sys.argv[0])))
sys.stdout = codecs.getwriter("utf-8")(sys.stdout)

from bottle import Bottle, route, run, template, static_file, redirect, request, HTTPResponse, ServerAdapter, response
from bottle import TEMPLATE_PATH, jinja2_template as template
# from program_controller import ProgramController
from utils import Config, h
import socket
from bottle_login import LoginPlugin
from user_model import UserModel
from models import ModelController

# todo 初期設定
# ユーザ一覧のデータベースファイルパス
user_path = "db/users.db"
# 鍵の作り方
# https://blog.motikan2010.com/entry/2017/01/11/Python%E3%81%A0%E3%81%91%E3%81%A7HTTPS%E3%82%B5%E3%83%BC%E3%83%90
IS_SSL = False
# 非同期処理
IS_ASYNCHRONOUS = False
# HTMLファイル等のURL
CONTENTS_DIR = "htdocs"
# ログインするかどうか
IS_LOGIN = True

# todo 初期化
# 初期化
#
# utils.py
# Config(path="ファイル保存先",section="セクション",value={キー:値})
config = Config(path="config.ini",
                section="root",
                value={
                    "host": "localhost",
                    "port": 8082,
                    "debug": 1,
                    "reloader": 1,
                    "ssl": 0,
                    "ssl_key_path": "",
                    "server": "wsgiref",
                })


def init():
    """
    問題なく初期化出来た場合はTrueを返す
    :return:
    """
    return True


# 非同期処理
if IS_ASYNCHRONOUS:
    from gevent import monkey

    monkey.patch_all()

if IS_SSL:
    class SSLWebServer(ServerAdapter):
        def run(self, handler):
            from gevent.pywsgi import WSGIServer
            srv = WSGIServer((self.host, self.port), handler,
                             certfile='./server.pem',
                             keyfile='./server.pem')
            srv.serve_forever()

# todo Bottleの初期化
root = Bottle()
APP_DIR = os.path.dirname(os.path.abspath(__file__))
TEMPLATE_PATH.append(APP_DIR)


# 別の場所にあるrouteをここでまとめる。
# from index import app as index_app
# root.mount('/index', index_app.app)


# todo エラー処理
# エラー処理
#

@root.error(404)
def error404(error):
    return """Not Found!<br/>{}""".format(error)


@root.error(500)
def error500(error):
    return "Server Error!<br/>{}".format(error)


# todo ログイン
# ログイン
#

root.config['SECRET_KEY'] = 'secret'
user = UserModel(user_path)
login = root.install(LoginPlugin())
user.use(IS_LOGIN)


def is_login_deco(func):
    """
    ログインしているかどうかを調べるデコレータ
    :param func:
    :return:
    """

    def wrapper(*args, **kwargs):
        if not user.is_use():
            return func(*args, **kwargs)
        current_user = login.get_user()
        if current_user is None:
            return redirect("/login")
        return func(*args, **kwargs)

    return wrapper


def is_admin_deco(func):
    """
    ユーザがadminかどうかを調べるデコレータ
    :param func:
    :return:
    """

    def wrapper(*args, **kwargs):
        if not user.is_use():
            return func(*args, **kwargs)
        current_user = login.get_user()
        if current_user is None:
            return redirect("/")
        elif user.is_admin(current_user):
            return func(*args, **kwargs)
        else:
            return redirect("/")

    return wrapper


@root.get("/login")
def login_page():
    """
    ログイン画面
    :return:
    """
    return template(CONTENTS_DIR + "/login")


@root.route("/do_login", method="POST")
def do_login():
    """
    ログインのAJAX
    :return:
    """
    username = h(request.forms.get("username"))
    password = h(request.forms.get("password"))
    if user.check_login(username, password):
        user_kp = user.get_kp(username, password)
        login.login_user(user_kp)
        return "success"
    else:
        return "failed"


@login.load_user
def load_user_by_id(user_id):
    """
    bottle_loginモジュールが使用する。
    :param user_id:
    :return:
    """
    return user_id


@root.route("/logout", method="GET")
def logout():
    """
    ログアウト
    :return:
    """
    login.logout_user()
    return redirect("/")


@root.route("/user_menu", method="POST")
def get_user_menu():
    """
    セッションからkpを取得してユーザ名を返す。
    :return:
    """
    if not user.is_use():
        return ""
    user_kp = login.get_user()
    if user_kp is None:
        return ""
    return user.get_user_name(user_kp)  # template("""{}""".format(current_user))


@root.route('/what_ip')
def hello():
    """
    IPアドレス取得のテストページ
    :return: メッセージ
    """
    client_ip = request.environ.get('REMOTE_ADDR')
    return ['Your IP is: {}\n'.format(client_ip)]


@root.route('/user_json', method="POST")
@is_admin_deco
def get_user_json():
    """
    DataTablesで表示するためのjsonを出力。
    :page: config.html,config.js
    :return: JSON
    :rtype: str
    """
    return user.get_dict_json(
        ["kp", "name"], "")


@root.route('/user_item', method="POST")
@is_admin_deco
def get_user_item():
    """
    userテーブルから１行検索してjsonで出力。モーダルウィンドウで編集用。
    :page: config.html,config.js
    :return: JSON
    :rtype: str
    """
    kp = request.forms.get("kp")
    return user.get_item_json(kp)


@root.route('/save_user', method="POST")
@is_admin_deco
def set_user():
    """
    userテーブルの内容保存
    :page: config.html,config.js
    :return:
    """
    return user.set_user(h(request.forms.get("kp")),
                         h(request.forms.get("name")),
                         h(request.forms.get("password")),
                         request.forms.get("role")
                         )


@root.route('/delete_user', method="POST")
@is_admin_deco
def delete_program():
    """
    userテーブルから１行選んで削除
    なお、roleがadminのものが１つでもないと削除出来ないようにしている。
    :return:
    """
    kp = request.forms.get("kp")
    return user.delete(kp)


# todo data
# indexページ
#
"""
ここでデータベースをアクセスしたり作成したりする。
コンテンツ関係はこの辺に記述
"""

# models.py
# 各データベースアクセス用のモジュール
# ModelController(file_path="sqliteのファイル",table_name="テーブル名",columns{キー:初期化})
example_controller = ModelController(
    file_path="db/example.db", table_name="example",
    columns={"data_name": "TEXT",
             "use": "INTEGER DEFAULT 1",
             "order_number": "INTEGER"
             })


@root.route('/')
@is_login_deco
def index():
    """
    index.html を表示
    :return: HTML
    :rtype: str
    """
    return template(CONTENTS_DIR + '/index')


@root.route('/index')
@is_login_deco
def index():
    """
    / にリダイレクト
    :return: redirect
    """
    return redirect('/')


@root.route('/list', method="GET")
@is_login_deco
def data_list():
    """
    JSON形式でデータを取得してそれを返す。
    :return: JSON
    :rtype: str
    """
    return example_controller.get_dict_json(["kp", "data_name"])


@root.route('/add', method="POST")
@is_login_deco
def data_add():
    """
    データを作成
    :return: JSON
    :rtype: str
    """
    username = h(request.forms.get("data_name"))
    example_controller.set({"data_name": username})
    return "add"


@root.route('/update', method="POST")
@is_login_deco
def data_update():
    """
    データを作成
    :return: JSON
    :rtype: str
    """
    object_data = {
        "kp": h(request.forms.get("kp")),
        "data_name": h(request.forms.get("data_name"))
    }
    example_controller.set(object_data)
    return "update"


@root.route('/item', method="POST")
@is_login_deco
def data_item():
    """
    JSON形式でデータを取得してそれを返す。
    :return: JSON
    :rtype: str
    """
    return example_controller.get_item_json(h(request.forms.get("kp")))


@root.route('/delete', method="POST")
@is_login_deco
def data_delete():
    kp = h(request.forms.get("kp"))
    example_controller.delete(kp)
    return ""


# todo Static
# Static Files
# フォルダの中身等を直接アクセスするためのメソッド
#

@root.route("/assets/<file_path:path>", name="static_file")
def static(file_path):
    """
    静的ファイルのパス。
    ポイントとしては、routeのパスと、static_fileのrootを一致させること。
    rootは関数にすることで動的に変更が可能。
    :param file_path: ファイルパス
    :type file_path:
    :return:
    """
    return static_file(file_path, root=os.path.join(APP_DIR, CONTENTS_DIR, "assets"))


@root.route("/js/<file_path:path>", name="static_file")
def static(file_path):
    return static_file(file_path, root=os.path.join(APP_DIR, CONTENTS_DIR, "js"))


# ポートスキャン
def port_scan():
    """
    ポート番号の重複を確認
    :return: ポートが重複して「いる」
    :rtype: bool
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    return_code = sock.connect_ex((config["host"], int(config["port"])))
    sock.close()
    # socket.connect_ex は成功すると0を返す
    if return_code == 0:
        return True
    else:
        return False


#
# main
#
def main():
    # 初期化
    if not init():
        # 初期化に失敗
        return
    # ポートをスキャンして２重起動を防止
    if port_scan():
        return
    # run(app=root, host='localhost', port=8080, debug=True)
    """
    # SSLキーありの場合
    run(app=root, host='localhost', port=8081, debug=True, server=SSLWebServer, reloader=True)
    # 非同期処理ありの場合
    run(app=root, host='localhost', port=8082, debug=True, server="gevent", reloader=True)
    """
    # run(app=root, host=config.host, port=int(config.port), debug=config.debug, reloader=config.reloader)
    if IS_SSL:
        run(app=root, host=config["host"], port=config["port"], debug=config.get_bool("debug"),
            reloader=config.get_bool("reloader"), server=SSLWebServer)
    elif IS_ASYNCHRONOUS:
        run(app=root, host='localhost', port=config["port"], debug=config.get_bool("debug"),
            reloader=config.get_bool("reloader"), server="gevent")
    else:
        run(app=root, host=config["host"], port=config["port"], debug=config.get_bool("debug"),
            reloader=config.get_bool("reloader"))


# 実行
if __name__ == '__main__':
    main()
