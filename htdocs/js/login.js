'use strict';

// ajax
function toAjax(url, fd, doneFunc, errFunc = function () {
}) {
    let getHtml = $.ajax({
        type: 'POST', // or GET
        url: url,
        dataType: 'html',
        data: fd,
        contentType: false,
        processData: false
    });
    getHtml.done(function (response) {
        doneFunc(response);
    });
    getHtml.fail(function (response) {
        errFunc(response);
    });
}

window.addEventListener('load', function () {
    $(document).on('click', '#button-login', function () {
        let fd = new FormData();
        fd.append("username", $('#username').val());
        fd.append("password", $('#password').val());
        toAjax("/do_login", fd, function (response) {
            if (response === "success") {
                window.location.href = '/index';
            }
        });
    });
});