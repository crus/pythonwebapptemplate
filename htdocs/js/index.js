'use strict';

// ajax
function toAjax(url, fd, doneFunc, errFunc = function () {
}) {
    let getHtml = $.ajax({
        type: 'POST', // or GET
        url: url,
        dataType: 'html',
        data: fd,
        contentType: false,
        processData: false
    });
    getHtml.done(function (response) {
        doneFunc(response);
    });
    getHtml.fail(function (response) {
        errFunc(response);
    });
}

window.addEventListener('load', function () {
    // テーブル表示
    let exampleTable = $('#example-table').DataTable({
        "ajax": {
            "url": '/list',
            "type": "GET"
        },
        // "order": [[0, "asc"]], // 並べ替え
        "searching": false,
        "paging": false,
        "info": false,
        "ordering": false,
        "columnDefs": [
            {
                "targets": 0,
                "title": "",
                "data": "kp",
                // "visible": false, // 非表示
                "searchable": false // サーチなし
            },
            {
                "targets": 1,
                "title": "値",
                "defaultContent": "",
                "data": function (row, type, val, meta) {
                    return "<div style='background-color:#333;color:#FFF'>" + row["data_name"] + "</div>";
                }
            }
        ],
        'rowCallback': function (row, data, index) {
            // １行全体に影響を与える箇所
            $(row).css('background-color', '#888888');
        }
    });

    // テーブルの行をクリック
    $('#example-table tbody').on('click', 'tr', function () {
        let kp = exampleTable.row(this).data()["kp"];
        let fd = new FormData();
        fd.append("kp", kp);
        toAjax("/item", fd, function (response) {
            let response_object = JSON.parse(response);
            $("#edit-modal-input-kp").val(response_object["kp"]);
            $("#edit-modal-input-name").val(response_object["data_name"]);
            $("#edit-modal").modal("show");
        });
    });

    // ボタン
    $(document).on('click', '#button-add', function () {
        let fd = new FormData();
        fd.append("data_name", $("#name").val());
        toAjax('/add', fd, function (response) {
            console.log(response);
            exampleTable.ajax.reload();
        }, function (response) {
            console.log(response);
        });
    });

    $(document).on('click', '#edit-modal-button-delete', function () {
        let kp = $("#edit-modal-input-kp").val();
        if (kp === undefined || kp === "") {
            return;
        }
        let res = confirm("削除しますか？");
        if (res !== true) {
            return;
        }
        let fd = new FormData();
        fd.append("kp", kp);
        toAjax('/delete', fd, function (response) {
            console.log(response);
            exampleTable.ajax.reload();
            $("#edit-modal").modal("hide");
        }, function (response) {
            console.log(response);
        });
    });

    $(document).on('click', '#edit-modal-button-save', function () {
        let kp = $("#edit-modal-input-kp").val();
        if (kp === undefined || kp === "") {
            return;
        }
        let fd = new FormData();
        fd.append("kp", kp);
        fd.append("data_name", $("#edit-modal-input-name").val());
        toAjax('/update', fd, function (response) {
            console.log(response);
            exampleTable.ajax.reload();
            $("#edit-modal").modal("hide");
        }, function (response) {
            console.log(response);
        });
    });

    // 5秒ごとに再読込
    let intervals = setInterval(function () {
        exampleTable.ajax.reload();
    }, 5000);
});