#! env python
# -*- coding: utf-8 -*-

import os
import sys
from utils import Model, h
import uuid
import json
import datetime

# Python_console.models
# Copyright (c) 2019 mikimasayuki. All rights reserved.

__author__ = 'mm'
__status__ = 'production'
__version__ = '0.0.2'
__date__ = '2019/03/18'

__pychecker__ = 'no-callinit no-classattr'


class ModelController(object):
    def __init__(self, file_path="program.db", table_name="program", columns={"test": "TEXT"}, key_column="kp"):
        self.__ROOT = os.path.dirname(os.path.abspath(sys.argv[0]))
        self.__EXE_PATH = sys.executable
        self.__ENV_PATH = os.path.dirname(self.__EXE_PATH)
        self.__LOG = os.path.join(self.__ENV_PATH, 'log')
        self.__sqlite_path = file_path
        self.__table_name = table_name
        self.__columns = columns
        self.__key_column = key_column
        self.__init()
        return

    def __init(self):
        is_create = False
        if not os.path.exists(self.__sqlite_path):
            is_create = True
        else:
            db = Model(self.__sqlite_path)
            is_create = not db.is_table(self.__table_name)
        if is_create:
            db = Model(self.__sqlite_path)
            sql = ""
            for key in self.__columns.keys():
                sql = """{} {} {},""".format(sql, key, self.__columns[key])
            if db.create("""
CREATE TABLE IF NOT EXISTS {} (id INTEGER PRIMARY KEY AUTOINCREMENT,{} TEXT,{}created_at TIMESTAMP DEFAULT (datetime('now','localtime')),updated_at TIMESTAMP DEFAULT (datetime('now','localtime')));
                            """.format(self.__table_name, self.__key_column, sql), table=self.__table_name):
                return
        return

    @staticmethod
    def get_value_str(value):
        if isinstance(value, str):
            return "\"{}\"".format(value)
        if isinstance(value, bool):  # boolはintのサブクラスのため、先にboolかどうかを判別する
            return "1" if value else "0"
        if isinstance(value, int) or isinstance(value, float) or isinstance(value, complex):
            return "{}".format(str(value))
        if isinstance(value, datetime.datetime):
            return value.strftime("%Y/%m/%d %H:%M:%S")
        return value

    def get_list(self, column_list=[], terms="ORDER BY order_number ASC"):
        sql = ""
        if len(column_list) == 0:
            sql = "*"
        else:
            for i in column_list:
                sql = sql + i
                if i != column_list[-1]:
                    sql = sql + ","
        db = Model(self.__sqlite_path)
        rows = db.select(
            """SELECT {} from {} {};""".format(sql, self.__table_name, terms))
        result = []
        for cols in rows:
            row_data = []
            for i in cols:
                row_data.append(i)
            result.append(row_data)
        return result

    def get_dict(self, column_list=[], terms="ORDER BY order_number ASC"):
        sql = ""
        if len(column_list) == 0:
            sql = "*"
        else:
            for i in column_list:
                sql = sql + i
                if i != column_list[-1]:
                    sql = sql + ","
        db = Model(self.__sqlite_path)
        rows = db.select(
            """SELECT {} from {} {};""".format(sql, self.__table_name, terms))
        result = []
        for cols in rows:
            row_data = {}
            for i in column_list:
                row_data[i] = cols[i]
            result.append(row_data)
        return result

    def get_list_json(self, column_list=[], terms="ORDER BY order_number ASC"):
        return json.dumps({"data": self.get_list(column_list, terms)})

    def get_item_json(self, kp):
        return json.dumps(self.get_item(kp))

    def get_dict_json(self, column_list=[], terms="ORDER BY order_number ASC"):
        return json.dumps({"data": self.get_dict(column_list, terms)})

    def get_item(self, key_column=""):
        db = Model(self.__sqlite_path)
        rows = db.select(
            """SELECT * from {} WHERE {} = '{}';""".format(self.__table_name, self.__key_column, key_column))
        return {key: rows[0][key] for key in rows[0].keys()}

    def execute(self, sql):
        db = Model(self.__sqlite_path)
        db.execute(sql)

    def delete(self, key_column):
        db = Model(self.__sqlite_path)
        rows = db.select(
            """SELECT kp from {} WHERE {} = '{}';""".format(self.__table_name, self.__key_column, key_column))
        if len(rows) != 0:
            db.execute("""DELETE FROM {} WHERE {} ='{}'""".format(self.__table_name, self.__key_column, key_column))

    def set(self, value_dict={"kp": ""}):
        """
        example
        value_dict = {"program_name",request.forms.get("program_name")}
        :param value_dict:
        :return:
        """
        if len(value_dict) == 0:
            return
        # 個別idが無ければUUIDを指定
        kp = ""
        if not self.__key_column in value_dict.keys():
            kp = str(uuid.uuid4())
        elif value_dict[self.__key_column] == "":
            kp = str(uuid.uuid4())
        else:
            kp = value_dict[self.__key_column]
        db = Model(self.__sqlite_path)
        rows = db.select("""SELECT kp from {} WHERE kp = '{}';""".format(self.__table_name, kp))
        if len(rows) == 0:
            sql_key = ""
            sql = ""
            tmp_dict = value_dict
            tmp_dict[self.__key_column] = kp
            keys = list(tmp_dict.keys())
            for i in keys:
                sql = sql + self.get_value_str(tmp_dict[i])
                sql_key = sql_key + i
                if i != keys[-1]:
                    sql = sql + ","
                    sql_key = sql_key + ","
            save_db = Model(self.__sqlite_path)
            save_db.execute("""
INSERT INTO {} ({}) VALUES ({});""".format(self.__table_name, sql_key, sql))
        else:
            sql = ""
            tmp_dict = value_dict
            tmp_dict["updated_at"] = datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")
            # kp削除
            del tmp_dict[self.__key_column]
            keys = list(tmp_dict.keys())
            for i in keys:
                sql = sql + i + "=" + self.get_value_str(tmp_dict[i])
                if i != keys[-1]:
                    sql = sql + ","
            save_db = Model(self.__sqlite_path)
            save_db.execute("""
UPDATE {} SET {} WHERE {} = '{}';""".format(self.__table_name, sql, self.__key_column, kp))
        return ""
