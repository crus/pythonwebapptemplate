#! env python
# -*- coding: utf-8 -*-

import os
import sys
# from utils import Model
from models import ModelController, Model
import uuid
import json

# Python_console.user_controller
# Copyright (c) 2019 mikimasayuki. All rights reserved.

__author__ = 'mm'
__status__ = 'production'
__version__ = '0.0.1'
__date__ = '2019/03/14'

__pychecker__ = 'no-callinit no-classattr'


class UserModel(object):
    def __init__(self, path):
        """
        :param path:
        """
        self.__ROOT = os.path.dirname(os.path.abspath(sys.argv[0]))
        self.__EXE_PATH = sys.executable
        self.__ENV_PATH = os.path.dirname(self.__EXE_PATH)
        self.__LOG = os.path.join(self.__ENV_PATH, 'log')
        self.__path = path
        self.__table_name = "user"
        self.__is_use = False
        self.__model = None
        self.__init()
        return

    def __init(self):
        """
        データベース作成
        :return:
        """
        is_first = False
        if not os.path.exists(self.__path):
            is_first = True
        self.__model = ModelController(
            file_path=self.__path, table_name="user",
            columns={"name": "TEXT",
                     "password": "TEXT",
                     "role": "TEXT",
                     "use": "INTEGER DEFAULT 1",
                     })
        if is_first:
            self.__model.set(
                {"name": "admin", "password": "admin", "role": "admin"}
            )

    def use(self, value):
        """
        ログイン機能の設定
        :param value:
        :return:
        """
        self.__is_use = value

    def is_use(self):
        """
        ログイン機能を使うかどうか。
        :return:
        """
        return self.__is_use

    def check_login(self, user, password):
        """
        ログイン画面からの判定に使用。
        :param user:
        :param password:
        :return:
        """
        return len(self.__model.get_list(
            ["name"], "WHERE name = '{}' AND password = '{}'".format(user, password))
        ) > 0

    def get_kp(self, user_name, password):
        """
        user_nameとpasswordからkpを取得
        :param user_name:
        :param password:
        :return:
        """
        kp = self.__model.get_list(
            ["kp"], "WHERE name = '{}' AND password = '{}'".format(user_name, password))
        return kp[0][0]

    def get_user_name(self, kp):
        """
        kpからユーザ名を取得
        :param kp:
        :return:
        """
        user_name = self.__model.get_list(
            ["name"], "WHERE kp = '{}'".format(kp))
        return user_name[0][0]

    def set_user(self, kp, user_name, password, role):
        """
        ユーザ登録。kpが空の場合は新規登録
        :param role:
        :param kp:
        :param user_name:
        :param password:
        :return:
        """
        test = self.__model.get_list(["name"], "WHERE name = '{}'".format(user_name))
        is_other_user = len(self.__model.get_list(["name"], "WHERE name = '{}'".format(user_name))) != 0
        is_kp = not (kp == "" or kp is None)
        if is_other_user and is_kp:
            return "failed"
        kp_data = str(uuid.uuid4()) if is_kp else kp
        self.__model.set({
            "kp": kp_data,
            "name": user_name,
            "password": password,
            "role": role,
        })
        return "success"

    def is_admin(self, kp):
        """
        ユーザのroleがadminかどうかを判定
        :param kp:
        :return:
        """
        data = self.__model.get_list(
            ["role"], "WHERE kp = '{}'".format(kp))
        if len(data) == 0:
            return False
        return data[0][0] == "admin"

    def get_list(self, column_list=[], terms="ORDER BY order_number ASC"):
        sql = ""
        if len(column_list) == 0:
            sql = "*"
        else:
            for i in column_list:
                sql = sql + i
                if i != column_list[-1]:
                    sql = sql + ","
        db = Model(self.__path)
        rows = db.select(
            """SELECT {} from {} {};""".format(sql, self.__table_name, terms))
        result = []
        for cols in rows:
            row_data = []
            for i in cols:
                row_data.append(i)
            result.append(row_data)
        return result

    def get_dict(self, column_list=[], terms="ORDER BY order_number ASC"):
        sql = ""
        if len(column_list) == 0:
            sql = "*"
        else:
            for i in column_list:
                sql = sql + i
                if i != column_list[-1]:
                    sql = sql + ","
        db = Model(self.__path)
        rows = db.select(
            """SELECT {} from {} {};""".format(sql, self.__table_name, terms))
        result = []
        for cols in rows:
            row_data = {}
            for i in column_list:
                row_data[i] = cols[i]
            result.append(row_data)
        return result

    def get_list_json(self, column_list=[], terms="ORDER BY order_number ASC"):
        return json.dumps({"data": self.get_list(column_list, terms)})

    def get_dict_json(self, column_list=[], terms="ORDER BY order_number ASC"):
        return json.dumps({"data": self.get_dict(column_list, terms)})

    def get_item_json(self, kp):
        return json.dumps(self.get_item(kp))

    def get_item(self, kp=""):
        db = Model(self.__path)
        rows = db.select("""SELECT * from {} WHERE kp = '{}';""".format(self.__table_name, kp))
        return {key: rows[0][key] for key in rows[0].keys()}

    def delete(self, kp):
        db = Model(self.__path)
        rows = db.select("""SELECT role from {} WHERE kp = '{}';""".format(self.__table_name, kp))
        if len(rows) != 0:
            if rows[0][0] == "admin":
                # adminのroleが１つでもないと削除出来ないようにする。
                rows = db.select("""SELECT role from {} WHERE role = 'admin';""".format(self.__table_name))
                if len(rows) <= 1:
                    return
            db.execute("""DELETE FROM {} WHERE kp ='{}'""".format(self.__table_name, kp))
